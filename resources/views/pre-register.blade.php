@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('ลงทะเบียนผู้สมัคร') }}</div>

                    <div class="card-body">
                        {{-- {{ __('register page') }} --}}

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <a href={{ route('register.group.show') }} type="button"
                            class="btn btn-primary">ลงสมัครเป็นคนแรกของพรรค</a>
                        <a href={{ route('register') }} class="btn btn-success">มีพรรคเเล้ว</a>

                    </div>

                </div>
            </div>
        </div>
        <div class="row justify-content-center my-5">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('สุ่มคะแนน') }}</div>
                    <div class="card-body">
                        @if (Session::get('fail'))
                            <div class="alert alert-danger">
                                {{ Session::get('fail') }}
                            </div>
                        @endif
                        @if (Session::get('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        {{-- @if (isset($getName)) --}}
                        @foreach ($getName as $item)
                            @if ($item->amount < 11)
                                <button type="submit" class="btn btn-warning disabled">สุ่มการลงคะแนนเลือกตั้ง</button>
                            @else
                                <form action={{ route('reset') }} method="get">
                                    @csrf
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-warning">สุ่มการลงคะแนนเลือกตั้ง</button>
                                </form>
                            @endif
                        @endforeach
                        {{-- @endif --}}
                    </div>
                </div>

            </div>
        </div>
        <div class="row justify-content-center my-5">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-white bg-success">{{ __('พรรคที่ได้คะแนนสูงสุด') }}</div>
                    <div class="card-body">
                        @if (isset($getName))

                            @foreach ($getName as $item)

                                @if ($item->amount == 11)
                                    <h5>เลขที่พรรค : {{ $item->id }}</h5> <br>
                                    <h5>ชื่อพรรค : {{ $item->groupname }}</h5>
                                @endif
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>
        </div>
        <div class="row justify-content-center my-5">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('รายชื่อพรรคการเมืองทั้งหมด') }}</div>
                    <div class="card-body">
                        @if (isset($groupnames))

                            @foreach ($groupnames as $groupname)
                                <div class="row inline-block ">
                                    <div class="col-lg-6">
                                        <p>เลขที่พรรค : {{ $groupname->id }}</p>
                                    </div>
                                    <div class="col-lg-6">
                                        <p>ชื่อพรรค : {{ $groupname->groupname }}</p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>
        </div>
        <div class="row justify-content-center my-5">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('รายชื่อพรรคการเมืองทั้งหมด') }}</div>
                    <div class="card-body">
                        @if (isset($getRepresentatives))
                            <table id="example" class="table table-striped table-bordered " width="100%">
                                <thead>
                                    <tr>
                                        <th scope="col">เลขที่</th>
                                        <th scope="col">ชื่อจริง</th>
                                        <th scope="col">นามสกุล</th>
                                        <th scope="col">พรรค</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($getRepresentatives as $getRepresentative)
                                        <tr>
                                            <th scope="row">{{ $getRepresentative->id }}</th>
                                            <td>{{ $getRepresentative->firstname }}</td>
                                            <td>{{ $getRepresentative->lastname }}</td>
                                            <td>{{ $getRepresentative->group->groupname }}</td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>

                        @endif
                    </div>

                </div>
            </div>
        </div>
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#example').DataTable();
            });

        </script>

    @endsection
