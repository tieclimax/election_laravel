DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `selected_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;


DROP TABLE IF EXISTS `representatives`;
-- --------------------------------------------------------

--
-- Table structure for table `representatives`
--

CREATE TABLE `representatives` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edu_level` enum('ม.6','ปวช.','ปวส.','อนุปริญญา','ปริญญาตรี','ปริญญาโท','ปริญญาเอก') COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `representatives`
--

INSERT INTO `representatives` (`id`, `firstname`, `lastname`, `edu_level`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 'Jocelyn', 'Stephens', 'ปริญญาเอก', 1, '2021-03-21 02:10:06', '2021-03-21 02:10:06'),
(2, 'Anika', 'Phelps', 'ปริญญาตรี', 1, '2021-03-21 02:10:11', '2021-03-21 02:10:11'),
(3, 'Chadwick', 'Townsend', 'ปวส.', 1, '2021-03-21 02:10:19', '2021-03-21 02:10:19'),
(4, 'Igor', 'Joseph', 'ปวช.', 1, '2021-03-21 02:10:26', '2021-03-21 02:10:26'),
(5, 'Shaeleigh', 'Gomez', 'ม.6', 1, '2021-03-21 02:10:32', '2021-03-21 02:10:32'),
(6, 'Matthew', 'Jones', 'ม.6', 1, '2021-03-21 02:10:36', '2021-03-21 02:10:36'),
(7, 'Galvin', 'Best', 'ปริญญาตรี', 1, '2021-03-21 02:10:42', '2021-03-21 02:10:42'),
(8, 'Eleanor', 'Patrick', 'ปริญญาโท', 1, '2021-03-21 02:10:45', '2021-03-21 02:10:45'),
(9, 'Fredericka', 'Giles', 'ปริญญาตรี', 1, '2021-03-21 02:10:48', '2021-03-21 02:10:48'),
(10, 'Axel', 'Burton', 'ม.6', 1, '2021-03-21 02:10:52', '2021-03-21 02:10:52'),
(11, 'Latifah', 'Morin', 'ปวช.', 1, '2021-03-21 02:10:57', '2021-03-21 02:10:57'),
(12, 'Baxter', 'Dyer', 'ปริญญาโท', 1, '2021-03-21 02:11:02', '2021-03-21 02:11:02'),
(13, 'Cairo', 'Owens', 'ม.6', 2, '2021-03-21 02:12:28', '2021-03-21 02:12:28'),
(14, 'Quincy', 'Cantrell', 'ปริญญาตรี', 2, '2021-03-21 02:12:41', '2021-03-21 02:12:41'),
(15, 'Christopher', 'Farley', 'ปริญญาเอก', 2, '2021-03-21 02:12:50', '2021-03-21 02:12:50'),
(16, 'Bruce', 'Gallegos', 'ปริญญาเอก', 2, '2021-03-21 02:12:54', '2021-03-21 02:12:54'),
(17, 'Dawn', 'Vasquez', 'ปวส.', 2, '2021-03-21 02:12:57', '2021-03-21 02:12:57'),
(18, 'Willa', 'Robbins', 'ปริญญาเอก', 2, '2021-03-21 02:13:06', '2021-03-21 02:13:06'),
(19, 'Keaton', 'Griffith', 'ปริญญาโท', 2, '2021-03-21 02:13:12', '2021-03-21 02:13:12'),
(20, 'Marshall', 'Banks', 'ม.6', 2, '2021-03-21 02:13:16', '2021-03-21 02:13:16'),
(21, 'Ava', 'Newman', 'ม.6', 2, '2021-03-21 02:13:21', '2021-03-21 02:13:21'),
(22, 'Shelly', 'Davis', 'ปวช.', 2, '2021-03-21 02:13:25', '2021-03-21 02:13:25'),
(23, 'Kiona', 'Tillman', 'ม.6', 2, '2021-03-21 02:13:30', '2021-03-21 02:13:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `representatives`
--
ALTER TABLE `representatives`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `representatives`
--
ALTER TABLE `representatives`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;


DROP TABLE IF EXISTS `groups`;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `groupname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `groupname`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'เพื่อไทย', 11, '2021-03-21 00:01:09', '2021-03-21 02:11:02'),
(2, 'อนาคตใหม่', 11, '2021-03-21 00:01:23', '2021-03-21 02:13:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

